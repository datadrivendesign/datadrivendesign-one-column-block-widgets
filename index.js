module.exports = {
  extend: 'apostrophe-widgets',
  label: 'One Column Block',
  contextualOnly: true,
  addFields: [
    {
      type: 'area',
      name: 'center',
      label: 'Center',
      contextual: true
    }
  ]
};
