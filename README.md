# README #

### What is this repository for? ###

* Adds a block to the editor allowing the user to create a bootstrap 12 column layout with 1 content areas.
* 1.0.0

### How do I get set up? ###


```
#!bash

npm install -save git+ssh://git@bitbucket.org:datadrivendesign/datadrivendesign-one-column-block-widgets.git
```


### How to use it? ###

```
#!javascript

{{ apos.area(data.page, 'area-name', {
    widgets: {
       'datadrivendesign-one-column-block': {}
    }
}) }}
```